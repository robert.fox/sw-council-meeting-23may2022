import setuptools

with open("README.md", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="dpn",
    version="0.0.1",
    description="Our pycon demo for the SW council meeting",
    long_description=long_description,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.11.0b1",
    ],
    url="https://gitlab.com/robert.fox/sw-council-meeting-23may2022",
    author="Luke, Robert and Helen",
    author_email="https://gitlab.com/robert.fox/sw-council-meeting-23may2022",
    license="MIT",
    package_dir={"": "."},
    packages=setuptools.find_packages(where="."),
    python_requires=">=3.11",
    install_requires=[
        "numpy",
    ],
    extras_require={
        "dev": [
            "pre-commit",
            "pytest",
        ],
    },
)
