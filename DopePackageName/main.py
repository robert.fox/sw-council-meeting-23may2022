import numpy as np

print("Ready for use!")


def point_matcher(point: tuple) -> str:

    return point_matcher2(point)


def point_matcher2(point: tuple) -> str:
    match point:
        case (0, 0):
            return "Origin"
        case (0, y):
            return f"Y={y}"
        case (x, 0):
            return f"X={x}"
        case (x, y):
            return f"X={x}, Y={y}"
        case _:
            raise ValueError("Not a point")
