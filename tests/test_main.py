import pytest

from DopePackageName.main import point_matcher, point_matcher2

locallist = [1, 2, 3]


@pytest.mark.parametrize(
    "point",
    [
        (0, 0),
        (1, 0),
        (0, 2.5),
        (-2, -3.7),
        ("string", "other string"),
        ("string", 0),
        [1, 2],
    ],
)
def test_point_matcher_matches_point_matcher_2_valid_inputs(point):

    assert point_matcher(point) == point_matcher2(point)


@pytest.mark.parametrize(
    "point", [(), (1, 0, -1), 0, ([1, 2],), "some random string", "ab"]
)
def test_point_matcher_and_point_matcher_2_raise_ValueError_on_invalid_inputs(point):

    with pytest.raises(ValueError):
        point_matcher(point)

    with pytest.raises(ValueError):
        point_matcher2(point)
